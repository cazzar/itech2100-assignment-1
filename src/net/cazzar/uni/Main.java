package net.cazzar.uni;

import net.cazzar.uni.jobs.manager.JobManager;
import net.cazzar.uni.menu.impl.MainMenu;

public class Main {
    private static JobManager jobManager;

    //if you dont know what psvm is, why?
    public static void main(String[] args) {
        jobManager = new JobManager();
        new MainMenu().show();
        //want to ask to exit
        //This is done in BaseMenu.
    }

    /**
     * Get the instance of JobManager that is used.
     *
     * @return the {@link net.cazzar.uni.jobs.manager.JobManager}
     */
    public static JobManager getJobManager() {
        return jobManager;
    }
}
