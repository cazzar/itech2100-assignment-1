package net.cazzar.uni.jobs.manager;

import net.cazzar.uni.jobs.IJob;
import net.cazzar.uni.jobs.Rate;
import net.cazzar.uni.jobs.exceptions.JobException;
import net.cazzar.uni.menu.BaseMenu;

import java.util.*;

/**
 * @author cayde
 */
public class JobManager {
    //A reference to all jobs carried over by day.
    private final Map<Day, List<IJob>> jobsByDay = new HashMap<>();
    private Day currentDay;
    private double paymentsToday = 0.0;

    public JobManager() {
        for (Day day : Day.values()) this.jobsByDay.put(day, new ArrayList<IJob>());
        this.currentDay = Day.SUNDAY;
    }

    public Day getDayForJob(IJob job) {
        for (Day day : Day.values()) if (this.jobsByDay.get(day).contains(job)) return day;

        return null;
    }

    public List<IJob> getJobsByDay(Day day) {
        //We cannot clone a generic interface, but, we can do this!.
        //Yes, I could just do (List<IJob>)((LinkedList<IJob>)this.jobsByDay.get(day)).clone()
        //But, honestly, you would agree that looks horrible.
        return new LinkedList<>(this.jobsByDay.get(day));
    }

    public boolean addPaymentForJob(IJob job, double amount) {
        //we need to check the amount of money we are paying against what is payable.
        if (job.getAmountPayable() > amount) {
            return false;
        }

        this.paymentsToday += amount;
        job.pay(amount);
        return true;
    }

    public void rollOver() {
        this.printStats();

        if (this.currentDay != Day.SUNDAY) {
            LinkedList<IJob> jobsToRemoveFromToday = new LinkedList<>();

            for (IJob job : this.jobsByDay.get(this.currentDay)) {
                System.out.printf("Processing job: %s", job);
                //hourly
                if (job.isStarted()) {
                    if (job.getChargingRate() == Rate.DAILY) {
                        final String choice = BaseMenu.choose("Was the job worked on today?", "yes", "no");
                        if ("yes".equalsIgnoreCase(choice)) job.addLabour(1, Rate.DAILY);
                    } else
                        job.addLabour(BaseMenu.getDouble("How much was the job worked on today?", "Please enter a double value"), Rate.HOURLY);

                    if (!job.isComplete() && "yes".equalsIgnoreCase(BaseMenu.choose("yes", "no"))) {
                        try {
                            job.completeWork();
                        } catch (JobException e) {
                            System.out.printf("Encountered a JobException.%s error%n", e.getClass().getSimpleName());
                        }
                    }
                }

                if (!job.isComplete()) {
                    System.out.printf("Rolling job %d over (%s)", job.getJobID(), job);
                    jobsToRemoveFromToday.add(job);

                    this.jobsByDay.get(this.currentDay.next()).add(job);
                }
            }

            //because, potential CMFE
            for (IJob toRemove : jobsToRemoveFromToday) {
                this.jobsByDay.get(this.currentDay).remove(toRemove);
            }
        }

        this.currentDay = this.currentDay.next();
        this.newDay();
    }

    public List<IJob> getJobsStillRequiringPayment() {
        List<IJob> unpaid = new LinkedList<>();
        for (List<IJob> iJobs : this.jobsByDay.values())
            for (IJob iJob : iJobs) if (iJob.isComplete() && iJob.getAmountPayable() > 0) unpaid.add(iJob);

        return unpaid;
    }

    private void newDay() {
        this.paymentsToday = 0;
        for (IJob job : this.getJobsByDay(this.currentDay)) {
            System.out.printf("Job id %d needs work for today.%n", job.getJobID());
        }
    }

    public void printStats() {
        System.out.println("Payments today: $" + this.paymentsToday);
    }

    public Day getCurrentDay() {
        return this.currentDay;
    }

    /**
     * Book a job for the day
     *
     * @param day the day to book for.
     * @param job the job to book
     * @return successful if true, else false.
     */
    public boolean bookJobForDay(Day day, IJob job) {
        if (day.ordinal() <= this.currentDay.ordinal()) {
            System.out.println("You cannot book a job for a day already past!");
            return false;
        }
        this.jobsByDay.get(day).add(job);
        return true;
    }

    @Override
    public String toString() {
        return "JobManager{" +
                "jobsByDay=" + this.jobsByDay +
                ", currentDay=" + this.currentDay +
                ", paymentsToday=" + this.paymentsToday +
                '}';
    }

    /**
     * We book on sundays
     */
    public static enum Day {
        SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY;
        private static final Day[] validBookingDays = new Day[]{MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY};

        public static Day[] getValidBookingDays() {
            return validBookingDays;
        }

        /**
         * Returns a cyclic term of the days therefore
         * {@code MONDAY -> TUESDAY -> WEDNESDAY -> THURSDAY -> FRIDAY -> MONDAY}
         *
         * @return the next day in the cycle.
         */
        public Day next() {
            return values()[(this.ordinal() + 1) % values().length];
        }
    }
}
