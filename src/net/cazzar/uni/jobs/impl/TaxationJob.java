package net.cazzar.uni.jobs.impl;

import net.cazzar.uni.Client;
import net.cazzar.uni.jobs.DailyJob;

/**
 * @author Cayde
 */
public class TaxationJob extends DailyJob {
    public TaxationJob(Client client) {
        super(client);
    }

    @Override
    public double getRate() {
        return 15;
    }

    @Override
    public double getInitialCost() {
        return 230;
    }
}
