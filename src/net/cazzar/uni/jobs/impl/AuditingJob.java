package net.cazzar.uni.jobs.impl;

import net.cazzar.uni.Client;
import net.cazzar.uni.jobs.DailyJob;

/**
 * @author Cayde
 */
public class AuditingJob extends DailyJob {
    public AuditingJob(Client client) {
        super(client);
    }

    @Override
    public double getInitialCost() {
        return 230;
    }

    @Override
    public double getRate() {
        return 15;
    }
}
