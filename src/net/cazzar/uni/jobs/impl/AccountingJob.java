package net.cazzar.uni.jobs.impl;

import net.cazzar.uni.Client;
import net.cazzar.uni.jobs.HourlyJob;

/**
 * @author Cayde
 */
public class AccountingJob extends HourlyJob {
    public AccountingJob(Client client) {
        super(client);
    }

    @Override
    public double getInitialCost() {
        return 0;
    }

    @Override
    public double getRate() {
        return 30;
    }
}
