package net.cazzar.uni.jobs;

import net.cazzar.uni.Client;
import net.cazzar.uni.jobs.exceptions.JobException;

/**
 * @author Cayde
 */
public abstract class DailyJob implements IJob {
    private final Client client;
    private double amountOfLabour = 0.0; //0 days
    private boolean completed = false;
    private boolean started = false;
    private double paid = 0.0;

    /**
     * The main constructor for the job
     *
     * @param client the client the job is assigned to.
     */
    protected DailyJob(Client client) {
        this.client = client;
    }

    @Override
    public void addLabour(double amount, Rate rate) {
        this.amountOfLabour += rate == Rate.DAILY ? amount : amount / 24;
    }

    @Override
    public void completeWork() throws JobException {
        //can we complete work when we have not even started?
        if (!this.started) throw new JobException.NotStarted();
        //Throw so we already know.
        if (this.completed) throw new JobException.AlreadyCompleted();

        this.completed = true;
    }

    @Override
    public double getAmountPayable() {
        //Simple generic function.
        return (this.getInitialCost() + (this.getRate() * this.amountOfLabour)) - this.paid;
    }

    @Override
    public double getAmountPaid() {
        return this.paid;
    }

    @Override
    public Rate getChargingRate() {
        return Rate.DAILY;
    }

    @Override
    public boolean isStarted() {
        return this.started && !this.completed;
    }

    @Override
    public double pay(double amount) {
        double ret = 0;
        if (this.getAmountPayable() > amount) {
            ret = amount - this.getAmountPayable();
            this.paid += this.getAmountPayable();
            return ret;
        }
        this.paid += amount;
        return ret;
//        this.paid += amount;
//        return this.getAmountPayable();
    }

    @Override
    public boolean isComplete() {
        return this.completed;
    }

    @Override
    public void startWorking() throws JobException {
        if (this.started) throw new JobException.AlreadyStarted();
        if (this.completed) throw new JobException.AlreadyCompleted();

        this.started = true;
    }

    @Override
    public int getJobID() {
        //Why not? this is unique, isn't it?
        //Yes, this isn't the best for data-redundancy,
        //But, you did say, we didn't really need it?
        return this.hashCode();
    }

    @Override
    public Client getClient() {
        return this.client;
    }

    @Override
    public String toString() {
        return String.format("%s{id=%d, client=%s}", this.getClass().getSimpleName(), this.getJobID(), this.client);
    }
}
