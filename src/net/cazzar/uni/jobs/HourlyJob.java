package net.cazzar.uni.jobs;

import net.cazzar.uni.Client;
import net.cazzar.uni.jobs.exceptions.JobException;

/**
 * @author Cayde
 */
public abstract class HourlyJob implements IJob {
    private final Client client;
    private double amountOfLabour = 0.0; //0 days
    private boolean completed = false;
    private boolean started = false;
    private double paid = 0.0;

    protected HourlyJob(Client client) {
        this.client = client;
    }

    @Override
    public void addLabour(double amount, Rate rate) {
        this.amountOfLabour = rate == Rate.DAILY ? amount : amount * 24;
    }

    @Override
    public void completeWork() throws JobException {
        if (!this.started) throw new JobException.NotStarted();
        if (this.completed) throw new JobException.AlreadyCompleted();
        this.completed = true;
    }

    @Override
    public double getAmountPayable() {
        return this.getInitialCost() + (this.amountOfLabour * this.getRate()) - this.paid;
    }

    @Override
    public double getAmountPaid() {
        return this.paid;
    }


    @Override
    public Rate getChargingRate() {
        return Rate.HOURLY;
    }

    @Override
    public boolean isStarted() {
        return this.started;
    }

    @Override
    public double pay(double amount) {
        double ret = 0;
        if (this.getAmountPayable() > amount) {
            ret = amount - this.getAmountPayable();
            this.paid += this.getAmountPayable();
            return ret;
        }
        this.paid += amount;
        return ret;
    }

    @Override
    public boolean isComplete() {
        return this.started && this.completed;
    }

    @Override
    public void startWorking() throws JobException {
        if (this.started) throw new JobException.AlreadyStarted();
        if (this.completed) throw new JobException.AlreadyCompleted();

        this.started = true;
    }

    @Override
    public Client getClient() {
        return this.client;
    }

    @Override
    public int getJobID() {
        return this.hashCode();
    }

    @Override
    public String toString() {
        return String.format("%s{id=%d, client=%s}", this.getClass().getSimpleName(), this.getJobID(), this.client);
    }
}
