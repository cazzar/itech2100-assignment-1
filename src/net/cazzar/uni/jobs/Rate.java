package net.cazzar.uni.jobs;

/**
 * Used to reference if the task is paid Hourly or Daily
 *
 * @author Cayde
 */
//Honestly, this could be an inner class somewhere, or a boolean, but, it is here for expansion's sake!
public enum Rate {
    HOURLY, DAILY
}
