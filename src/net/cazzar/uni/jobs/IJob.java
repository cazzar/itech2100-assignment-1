package net.cazzar.uni.jobs;

import net.cazzar.uni.Client;
import net.cazzar.uni.jobs.exceptions.JobException;

/**
 * Provides the basic information about the job, allowing for some small bits of information
 *
 * @author Cayde
 */
public interface IJob {
    /**
     * Hey? We can work for half a day?
     * Add a specified amount of labour for the job
     *
     * @param amount the amount of labour to add onto a job
     * @param rate   the rate of the labour to be added, assuming the rate is automatically converted
     */
    public void addLabour(double amount, Rate rate);

    /**
     * Complete the work and mark the job as done, though payments may be pending depending on the job type.
     *
     * @throws net.cazzar.uni.jobs.exceptions.JobException When the job has already been completed.
     */
    public void completeWork() throws JobException;

    /**
     * Gets the amount of money still due for the current job
     *
     * @return the amount of due money.
     */
    public double getAmountPayable();

    /**
     * Get the amount that has been paid to a job
     * @return the amount that has been paid
     */
    public double getAmountPaid();

    /**
     * Get the rate this type of job is charged at (generally static for the job type)
     *
     * @return Either HOURLY or DAILY as per the rate the job is charged at.
     */
    public Rate getChargingRate();

    public double getInitialCost();

    public double getRate();

    /**
     * Is the job completed, can be used at end of day checks to see if a rollover is necessary
     *
     * @return If the job is complete or not
     */
    public boolean isComplete();

    /**
     * Called to check when the job is being actively worked on
     *
     * @return Weather the job has been started yet
     */
    public boolean isStarted();

    /**
     * Pay an amount towards the job
     *
     * @param amount the amount to change the balance by
     * @return the amount over drawn for the transaction
     */
    public double pay(double amount);

    /**
     * Called to start work on a job.
     *
     * @throws JobException When the job has already been started
     */
    public void startWorking() throws JobException;

    /**
     * Called to get the job ID (For my implementation I am using the HashCode which is unique for every
     *
     * @return the job ID
     */
    public int getJobID();

    public Client getClient();
}
