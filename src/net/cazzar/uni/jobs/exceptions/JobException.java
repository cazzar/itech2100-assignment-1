package net.cazzar.uni.jobs.exceptions;

/**
 * The base class for all job-related exceptions
 *
 * @author Cayde
 */
public class JobException extends Exception {

    /**
     * Used to signify that the Job is already started
     */
    public static class AlreadyStarted extends JobException {
    }

    /**
     * Used to signify that the job has already been completed.
     */
    public static class AlreadyCompleted extends JobException {
    }

    /**
     * Used to signify that the said job is not yet started.
     */
    public static class NotStarted extends JobException {
    }
}
