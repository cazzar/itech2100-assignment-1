package net.cazzar.uni.menu.impl;

import net.cazzar.uni.menu.BaseMenu;

/**
 * @author Cayde
 */
public class MainMenu extends BaseMenu {
    /**
     * Show the admin menu allowing rollover and job interaction
     */
    @MenuItem("Administration Menu")
    public void adminMenu() {
        new AdministrationMenu().show();
    }

    /**
     * Show the create booking menu
     */
    @MenuItem("Create a new booking")
    public void createBooking() {
        new CreateBooking().show();
    }

    @Override
    protected String getExitName() {
        return "Exit";
    }

    @Override
    protected boolean isMainMenu() {
        return true;
    }
}
