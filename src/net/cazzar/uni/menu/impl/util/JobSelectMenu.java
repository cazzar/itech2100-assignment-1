package net.cazzar.uni.menu.impl.util;

import net.cazzar.uni.Main;
import net.cazzar.uni.jobs.IJob;
import net.cazzar.uni.jobs.manager.JobManager;
import net.cazzar.uni.menu.BaseMenu;

import java.util.List;

/**
 * @author Cayde
 */
public class JobSelectMenu extends BaseMenu {
    private final JobManager.Day day;
    private final List<IJob> jobs;
    private IJob job;

    /**
     * Create a selection menu
     *
     * @param day the day to list the jobs by.
     */
    public JobSelectMenu(JobManager.Day day) {
        this.day = day;
        this.jobs = Main.getJobManager().getJobsByDay(this.day);
    }

    /**
     * Create a selection menu
     *
     * @param jobs the jobs to select from
     */
    public JobSelectMenu(List<IJob> jobs) {
        this.day = null;
        this.jobs = jobs;
    }

    @Override
    public boolean show() {
        int i = 0;
        if (this.jobs.size() == 0) {
            //Check if it is the current day.
            if (this.day == Main.getJobManager().getCurrentDay()) System.out.println("There are no jobs for Today");
                //Or is it not set?
            else if (this.day == null) System.out.println("There are no jobs to list");
                //Well, we have nothing else to do now.
            else System.out.println("There are no jobs for " + toTitleCase(this.day));
        } else {
            for (i = 0; i < this.jobs.size(); i++) {
                //Print out all the jobs.
                IJob job = this.jobs.get(i);
                System.out.printf("%d) %s%n", i + 1, job);
            }
        }

        //I like SOME aesthetics.
        System.out.println();
        System.out.printf("0) %s%n", this.getExitName());

        //I am loving this function, can you tell?
        int choice = getBoundedInt("Please enter a choice", 0, i);
        //Set the job if not
        if (choice != 0) {
            this.job = this.jobs.get(choice - 1);
            return true;
        }
        return false;
    }

    /**
     * Get the user-selected job
     *
     * @return the job, null if not selected (back)
     */
    public IJob getResult() {
        return this.job;
    }

    @Override
    public String toString() {
        return "JobSelectMenu{" +
                "day=" + this.day +
                ", job=" + this.job +
                ", jobs=" + this.jobs +
                '}';
    }
}
