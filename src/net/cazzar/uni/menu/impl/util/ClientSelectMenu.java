package net.cazzar.uni.menu.impl.util;

import net.cazzar.uni.Client;
import net.cazzar.uni.menu.BaseMenu;

import java.util.List;

/**
 * @author Cayde
 */
public class ClientSelectMenu extends BaseMenu {
    private Client result;

    @Override
    public boolean show() {
        List<Client> clients = Client.getAllClients();
        int i;
        for (i = 0; i < clients.size(); i++) {
            Client client = clients.get(i);
            System.out.printf("%d) %s %s (%s)%n", i + 1, client.getFirstName(), client.getLastName(), Integer.toHexString(client.hashCode()));
        }

        System.out.printf("%d) Create new Client%n", ++i);

        System.out.println();
        System.out.printf("0) %s%n", this.getExitName());

        int choice = getBoundedInt("Please enter a choice", 0, i);
        if (choice != 0) {
//                this.result = Client.getClient(choice);

            if (choice == i) {
                this.result = this.createNewClient();
            } else {
                this.result = clients.get(choice - 1);
            }
            return true;
        }
        return false;
    }

    /**
     * Create a new client prompting for the needed data.
     *
     * @return a new instance of Client.
     */
    private Client createNewClient() {
        final String firstName = getString("First Name", false);
        final String lastName = getString("Last Name", true);
        final String address = getString("Address", true);
        final String phone = getString("Phone", true);

        return new Client(firstName, lastName, address, phone);
    }

    /**
     * Get the final result of the menu selection, should never be null unless errors occur.
     *
     * @return the selected client that the user chose
     */
    public Client getResult() {
        return this.result;
    }

    @Override
    public String toString() {
        return "ClientSelectMenu{" +
                "result=" + this.result +
                '}';
    }
}
