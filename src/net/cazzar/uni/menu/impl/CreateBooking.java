package net.cazzar.uni.menu.impl;

import net.cazzar.uni.Client;
import net.cazzar.uni.Main;
import net.cazzar.uni.jobs.IJob;
import net.cazzar.uni.jobs.impl.AccountingJob;
import net.cazzar.uni.jobs.impl.AuditingJob;
import net.cazzar.uni.jobs.impl.TaxationJob;
import net.cazzar.uni.jobs.manager.JobManager;
import net.cazzar.uni.menu.BaseMenu;
import net.cazzar.uni.menu.impl.util.ClientSelectMenu;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * @author Cayde
 */
class CreateBooking extends BaseMenu {
    private static final String format = "Created a new booking of id %d for %s%n";

    /**
     * Create a taxation service booking with either a new or existing client.
     */
    @BaseMenu.MenuItem("New Taxation Service Booking")
    public void newTaxation() {
        IJob job = this.makeBookingOfType(TaxationJob.class);

        //this has to be checked here due to ways java works to keep working with the menu.
        if (job == null) {
            System.out.println("An error occurred while creating the job");
            return;
        }
        JobManager.Day day = choose(JobManager.Day.getValidBookingDays());
        if (Main.getJobManager().bookJobForDay(day, job)) System.out.printf(format, job.getJobID(), job.getClient());
    }

    /**
     * Create a Auditing service booking with either a new or existing client.
     */
    @BaseMenu.MenuItem("New Auditing Service Booking")
    public void newAuditing() {
        IJob job = this.makeBookingOfType(AuditingJob.class);

        if (job == null) {
            System.out.println("An error occurred while creating the job");
            return;
        }
        JobManager.Day day = choose(JobManager.Day.getValidBookingDays());
        if (Main.getJobManager().bookJobForDay(day, job)) System.out.printf(format, job.getJobID(), job.getClient());
    }

    /**
     * Create a Accounting service booking with either a new or existing client.
     */
    @BaseMenu.MenuItem("New Accounting Service Booking")
    public void newAccounting() {
        IJob job = this.makeBookingOfType(AccountingJob.class);

        if (job == null) {
            System.out.println("An error occurred while creating the job");
            return;
        }
        JobManager.Day day = choose(JobManager.Day.getValidBookingDays());
        if (Main.getJobManager().bookJobForDay(day, job)) System.out.printf(format, job.getJobID(), job.getClient());
    }

    /**
     * This is made to reduce code duplication, which indecently breaks class direct references.
     *
     * @param type the type of IJob to instantiate, needs to have a (Lnet/cazzar/unj/Client;) constructor
     * @param <T>  Any class that extends IJob that has the specified ctor.
     * @return a new instance of the class with a supplied ctor.
     */
    private <T extends IJob> T makeBookingOfType(Class<T> type) {
        try {
            Constructor<T> ctor = type.getDeclaredConstructor(Client.class);

            final ClientSelectMenu selectMenu = new ClientSelectMenu();

            //Show the menu to select.
            if (selectMenu.show())
                return ctor.newInstance(selectMenu.getResult());
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            //silently ignore.
        }
        return null;
    }
}
