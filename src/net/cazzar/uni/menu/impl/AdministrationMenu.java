package net.cazzar.uni.menu.impl;

import net.cazzar.uni.Main;
import net.cazzar.uni.menu.BaseMenu;
import net.cazzar.uni.menu.impl.util.JobSelectMenu;

/**
 * @author Cayde
 */
class AdministrationMenu extends BaseMenu {
    @MenuItem("List jobs by day")
    public void listJobsByDay() {
        new ListJobsTask().show();
    }

    @MenuItem("Roll over the day (also prints daily stats)")
    public void rollOverDay() {
        Main.getJobManager().rollOver();
    }

    @MenuItem("Interact with a job for today")
    public void makeJobInteraction() {
        final JobSelectMenu selectMenu = new JobSelectMenu(Main.getJobManager().getCurrentDay());
        if (selectMenu.show())
            new JobInteractionMenu(selectMenu.getResult()).show();
    }

    @MenuItem("Print stats for today")
    public void showStats() {
        Main.getJobManager().printStats();
    }

    @MenuItem("Show unpaid jobs")
    public void showUnpaidJobs() {
        final JobSelectMenu menu = new JobSelectMenu(Main.getJobManager().getJobsStillRequiringPayment());
        if (menu.show())
            new JobInteractionMenu(menu.getResult()).show();
    }
}
