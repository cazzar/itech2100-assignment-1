package net.cazzar.uni.menu.impl;

import net.cazzar.uni.Client;
import net.cazzar.uni.Main;
import net.cazzar.uni.jobs.IJob;
import net.cazzar.uni.jobs.exceptions.JobException;
import net.cazzar.uni.menu.BaseMenu;

/**
 * @author Cayde
 */
class JobInteractionMenu extends BaseMenu {
    private final IJob job;

    public JobInteractionMenu(IJob job) {
        this.job = job;
    }

    /**
     * Add a payment to the selected job
     */
    @MenuItem("Add work to the job")
    public void payoutJob() {
        if (!Main.getJobManager().addPaymentForJob(this.job, getDouble("How much to pay was added to the job?", "Please enter a valid double")))
            System.out.println("There is too much of a payment sent for the job");
        System.out.printf("The amount still payable for this job is: $%.2f", this.job.getAmountPayable());
    }

    /**
     * Show the client information
     */
    @MenuItem("Client Information")
    public void clientInformation() {
        //lower JVM overhead like this.
        final Client client = this.job.getClient();
        System.out.printf("Client Name: %s %s%n", client.getFirstName(), client.getLastName());
        System.out.printf("Client address: %s%n", client.getAddress());
        System.out.printf("Client phone: %s%n", client.getPhone());
    }

    /**
     * Is the job completed? print it out to the user.
     */
    @MenuItem("Is completed?")
    public void isCompleted() {
        System.out.printf("The Job of type: %s is %scomplete%n", this.job.getClass().getSimpleName(), this.job.isComplete() ? "" : "not ");
    }

    /**
     * Show the detailed report of the job.
     */
    @MenuItem("Detailed report")
    public void detailedReport() {
        System.out.println("Client Information");
        this.clientInformation();
        System.out.println();
        System.out.println("Job Information");

        //add a space at the string split using camelCaseToString()
        System.out.printf("The job is of type: %s%n", camelCaseToString(this.job.getClass().getSimpleName()));
        System.out.printf("The job is: %s%n",
                (this.job.isComplete() && this.job.getAmountPayable() > 0) ? "Complete and not paid" :
                        (this.job.isComplete() && this.job.getAmountPayable() == 0) ? "Complete and paid" :
                                (!this.job.isStarted()) ? "Not started" : "Started"
        );

        System.out.printf("The initial amount for the job cost is: %.2f%n", this.job.getAmountPayable() + this.job.getAmountPaid());
        System.out.printf("The amount the user has paid for the job is: %.2f%n", this.job.getAmountPaid());
    }

    /**
     * Mark the job as completed (may fail)
     */
    @MenuItem("Mark completed")
    public void markComplete() {
        System.out.println("Attempting to mark the job completed");
        try {
            this.job.completeWork();
        } catch (JobException e) {
            System.err.printf("Encountered an error of type: %s%n", e.getClass().getSimpleName());
        }
    }


    /**
     * Mark the job as being worked on (may fail)
     */
    @MenuItem("Start work")
    public void startWorkOnJob() {
        System.out.println("Attempting to start work on the job");
        try {
            this.job.startWorking();
        } catch (JobException e) {
            System.err.printf("Encountered an error of type: %s%n", e.getClass().getSimpleName());
        }
    }

    /**
     * Show the type of work that the job is charged in.
     */
    @MenuItem("Base units for charge")
    public void workType() {
        System.out.println("The base units for charge on this job is: " + toTitleCase(this.job.getChargingRate().name()));
    }

    @Override
    public String toString() {
        return "JobInteraction{" +
                "job=" + this.job +
                '}';
    }
}
