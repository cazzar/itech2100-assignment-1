package net.cazzar.uni.menu.impl;

import net.cazzar.uni.Main;
import net.cazzar.uni.jobs.IJob;
import net.cazzar.uni.jobs.manager.JobManager;
import net.cazzar.uni.menu.BaseMenu;

/**
 * @author cayde
 */
public class ListJobsTask extends BaseMenu {
    @Override
    public boolean show() {
        //super.show();
        int choice = -1;
        while (choice != 0) {
            //List the days that can be chosen~
            for (JobManager.Day day : JobManager.Day.getValidBookingDays())
                System.out.printf("%d) %s%n", day.ordinal(), toTitleCase(day.name()));

            //add a shortcut for today.
            System.out.printf("%d) List jobs for today%n", JobManager.Day.values().length + 1);
            System.out.println();
            //And the option to exit.
            System.out.printf("0) %s%n", this.getExitName());

            choice = getBoundedInt("Please enter a choice", 0, JobManager.Day.getValidBookingDays().length + 1);

            //select the choice that the user wants.
            if (choice != 0 && choice != JobManager.Day.values().length + 1) {
                //-1 because, well, we are using a 0 indexed array
                this.listByDay(JobManager.Day.getValidBookingDays()[choice - 1]);
            } else if (choice == JobManager.Day.getValidBookingDays().length + 1) { // is this the "Today" option
                this.listByDay(Main.getJobManager().getCurrentDay());
            }
        }

        //With this menu, we don't care about t/f results.
        return false;
    }

    /**
     * Get all the jobs by a day and list them.
     *
     * @param day the day to list by.
     */
    private void listByDay(JobManager.Day day) {
        if (Main.getJobManager().getJobsByDay(day).size() == 0) {
            System.out.println("There are no jobs to list for " + toTitleCase(day));
            return;
        }

        System.out.printf("Jobs for %s%n", toTitleCase(day));

        for (IJob job : Main.getJobManager().getJobsByDay(day)) {
            System.out.println(job);
        }
    }
}
