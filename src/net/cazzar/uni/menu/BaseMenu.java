package net.cazzar.uni.menu;

import net.cazzar.uni.Main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * @author Cayde
 */
public class BaseMenu {
//    private static final Scanner scanner = new Scanner(System.in);
    /**
     * I am using a BufferedReader due to some of the edge-cases that have turned out with the scanner class
     */
    private static final BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    protected static int getBoundedInt(String prompt, int min, int max) {
        int i = getInt(prompt);

        while (i < min || i > max) {
            i = getInt(String.format("Please enter a valid int [%d,%d]", min, max));
        }

        return i;
    }

    /**
     * Choose a Object from the list, toString is used on Object.
     *
     * @param args the objects to choose from.
     * @param <T>  The type of object wanted.
     * @return the object selected by the user
     */
    @SafeVarargs
    protected static <T> T choose(T... args) {
        return choose("Please enter a choice [1, " + args.length + ']', args);
    }

    /**
     * Choose a Object from the list, toString is used on Object.
     *
     * @param args   the objects to choose from.
     * @param prompt the prompt to display to the user
     * @param <T>    The type of object wanted.
     * @return the object selected by the user
     */
    @SafeVarargs
    public static <T> T choose(String prompt, T... args) {
        for (int i = 0; i < args.length; i++) {
            System.out.printf("%d) %s%n", i + 1, toTitleCase(args[i].toString()));
        }

        System.out.println();

        //And to think, with this method, I cannot get a IOOB error.
        return args[getBoundedInt(prompt, 1, args.length) - 1];
    }

    /**
     * Since this has no possibility to be the wrong data type I will just check for String.isEmpty()
     *
     * @param prompt   the prompt to display to the user
     * @param nonEmpty if empty is allowed, true for allowing empty strings.
     * @return the string, empty is possible if allowed.
     */
    protected static String getString(String prompt, boolean nonEmpty) {
        try {
            String format = "%s: ";
            if (prompt.endsWith("?") || prompt.endsWith("!")) format = "%s ";

            System.out.printf(format, prompt);
            String line = in.readLine();

            while (line.isEmpty() && nonEmpty) {
                System.out.printf(format, prompt);
                line = in.readLine();
            }

            return line;
        } catch (IOException e) {
//            e.printStackTrace();
            return null;
        }
    }

    /**
     * Get a int with customisable error and also prompt,
     *
     * @param prompt the prompt to display to the user
     * @param error  the error message to display to the user on non-integer entry
     * @return the user-supplied integer
     */
    private static int getInt(String prompt, String error) {
        try {
            System.out.printf("%s: ", prompt);
            try {
                return Integer.parseInt(in.readLine());
            } catch (NumberFormatException ex) {
                return getInt(error, error);
            }
        } catch (IOException e) {
            return -100;
        }
    }

    /**
     * Get a double with customisable error and also prompt
     *
     * @param prompt the prompt to display to the user
     * @param error  the error message to display to the user on non-integer entry
     * @return the user-supplied double
     */
    public static double getDouble(String prompt, String error) {
        try {
            System.out.printf("%s: ", prompt);
            try {
                return Double.parseDouble(in.readLine());
            } catch (NumberFormatException ex) {
                return getInt(error, error);
            }
        } catch (IOException e) {
            return -100;
        }
    }

    //I so wish this was C then I could just have
    // static int getInt(String prompt, String error = "Please enter an integer value")

    /**
     * Get a string with a default prompt, deferring to {@link net.cazzar.uni.menu.BaseMenu#getInt(String, String)}
     *
     * @param prompt the prompt to display to the user
     * @return the typed integer value when successful
     */
    private static int getInt(String prompt) {
        return getInt(prompt, "Please enter an integer value");
    }

    protected static String toTitleCase(Object in) {
        String first = in.toString().charAt(0) + "";
        String last = in.toString().substring(1);
        return first.toUpperCase() + last.toLowerCase();
    }

    protected static String camelCaseToString(String camelCase) {
        StringBuilder buffer = new StringBuilder();
        final char[] chars = camelCase.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (Character.isUpperCase(chars[i]) && i != 0) {
                buffer.append(' ').append(chars[i]);
            } else if (Character.valueOf('_').equals(chars[i])) {
                buffer.append(' ');
            } else buffer.append(chars[i]);
        }
        return buffer.toString();
    }

    /**
     * Show the menu for any given class
     *
     * @return if the menu was successful.
     */
    public boolean show() {
        //This is generic to the JobManager
        System.out.printf("Today is %s%n", toTitleCase(Main.getJobManager().getCurrentDay()));
        System.out.println();
        //Strip out these two lines, and you can use this almost anywhere.

        int choice = -1;
        while (choice != 0) {

            if (this.getClass() == BaseMenu.class) throw new RuntimeException("You cannot run show() on BaseMenu");

            ArrayList<Method> methods = new ArrayList<>();

            //Calling getClass so super classes should work in theory.
            for (Method method : this.getClass().getDeclaredMethods()) {
                if (!method.isAnnotationPresent(MenuItem.class)) continue;
                methods.add(method);
            }

            methods.sort(new Comparator<Method>() {
                @Override
                public int compare(Method o1, Method o2) {
                    return o1.getAnnotation(MenuItem.class).value().compareTo(o2.getAnnotation(MenuItem.class).value());
                }
            });

            for (int i = 0; i < methods.size(); i++) {
                System.out.printf("%d) %s%n", i, methods.get(i).getAnnotation(MenuItem.class).value());
            }

            System.out.println();
            System.out.printf("0) %s%n", this.getExitName());

            choice = getBoundedInt("Please enter a choice", 0, methods.size());
            if (choice != 0) {
                try {
                    methods.get(choice - 1).invoke(this);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
            if (choice == 0 && this.isMainMenu()) {
                String confirm = getString("Do you want to exit (type yes to exit)?", true);
                if (!"yes".equalsIgnoreCase(confirm)) choice = -1; // this 'hacks' my for loop to continue
            }
        }
        return true;
    }

    /**
     * Set the prompt message for the exit menu option.
     *
     * @return the menu option to exit.
     */
    protected String getExitName() {
        return "Back";
    }

    /**
     * Is this menu the Main menu for the program?
     *
     * @return if it is the main menu of the system.
     */
    protected boolean isMainMenu() {
        return false;
    }


    /**
     * Hey! Look, I Should be printed in the dynamic menu system!
     */
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.METHOD)
    public @interface MenuItem {
        String value();
    }
}
