package net.cazzar.uni;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Cayde
 */
public class Client {
    private static final List<Client> clients = new LinkedList<>();
    private final String firstName;
    private final String lastName;
    private final String address;
    private final String phone;

    /**
     * Initialize a new instance of the client class.
     *
     * @param firstName the first name of the client
     * @param lastName  the last name of the client
     * @param address   the address of the client.
     * @param phone     the phone number of the client
     */
    public Client(String firstName, String lastName, String address, String phone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phone = phone;
        clients.add(this);
    }

    /**
     * You don't want to edit all the clients? fine! its not like it is a checked addition
     *
     * @return the client list.
     */
    public static List<Client> getAllClients() {
        return new ArrayList<>(clients);
    }

    //Some unused functions removed for the sake of smaller bytecodes.
//    public static Client getClient(int index) {
//        return clients.get(index);
//    }
//    public void deleteClient() {
//        clients.remove(this);
//    }

    /**
     * Get the first name of the client
     *
     * @return the first name that was set.
     */
    public String getFirstName() {
        return this.firstName;
    }

    /**
     * Get the last name of the client
     *
     * @return the client's last name
     */
    public String getLastName() {
        return this.lastName;
    }

    /**
     * Get the address that the client is set at
     *
     * @return the address the client lives at
     */
    public String getAddress() {
        return this.address;
    }

    @Override
    public String toString() {
        return String.format("Client{firstName='%s', lastName='%s', address='%s', phone='%s'}", this.firstName, this.lastName, this.address, this.phone);
    }

    /**
     * Get the phone number of the client that was set.
     * @return the phone number
     */
    public String getPhone() {
        return this.phone;
    }
}
